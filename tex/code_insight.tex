\chapter{Code Insight}

\section{Code Completion}

Code completion in Builder is provided by plug-ins.
Each plug-in may provide a different type of completion proposal.

\begin{figure}[h]
\caption{Code completion proposals displayed as you type}
\label{fig:completion}
\centering
\includegraphics[width=1\textwidth]{nodes/completion.pdf}
\end{figure}

The completion implementation used Builder in builder is provided by \verb|GtkSourceView|.
The code is originally in Builder and battle tested against extremely large completion proposal sets.
It scales extremely well under this scenario as is common with some completion providers like \verb|libclang|.

To provide completion proposals have your plugin register a completion provider implementation using the \verb|GtkSource.CompletionProvider| interface.
The provider is expected to return a \verb|Gio.ListModel| of proposals.
It is also responsible for converting the proposal item into a visible representation using \verb|GtkSource.CompletionCell|.

\subsection{Refiltering Proposals}

The \verb|GtkSource.CompletionProvider| interface allows for the provider to refilter the results instead of requerying the provider.
Some implementations may choose to employ special code to improve the performance here.
For example, you might only need to look at a subset of results if the new query word is longer than the previous query.

\subsection{Display Cells}

Each cell of a completion proposal may be set by the \verb|GtkSource.CompletionProvider|.
Generally, their is an icon cell to denote the type of completion proposal.
The left cell contains the "return type" of the proposal when there is one.
The typed-text cell contains the text which the user is expected to have typed.
The right cell contains the parameter list or other suppliemental data.

\subsection{Alternate Proposals}

Some languages contain multiple proposals for the same typed text.
One such example is languages which provide "overloads" for functions such as C++.
In this case, you may want to use "alternates" rather than a list of many items with the same typed text.
This allows the user to navigate through the results using arrow keys.

See \verb|GtkSource.CompletionProvider.list_alternates| for how to list alternates for a proposal.

\section{Diagnostics}

Diagnostics help developers find errors before they compile, or worse, the code gets in to production.
Therefore it is extremely important to get the diagnostics under the developers watchful eye.

Use \gidoc{iface}{DiagnosticProvider} to provide diagnostics while a file is being edited.
The results will be grouped together with other diagnostics and displayed to the user through the \hyperref[section:omni-gutter]{Onni Gutter}.

\subsection{Diagnostic Tools}

Many diagnostics require running a program on the developers computer.
That program might be installed as part of the project itself, expecially with some languages and tooling like \verb|npm|.
Additionally, the program might be bundled in an SDK or SDK-extension like with Flatpak-based projects.
The diagnostic program might even be expected to be installed on the developers host operating system.

To support all of these, and simplify the complicated process of locating the diagnostic tool and where it should run, \gidoc{class}{DiagnosticTool} was added.
It provides a simplified API to run tools and process their output.
They will attempt to run in the best environment supported for the project.

In many cases you need to simply provide a program name like "\verb|eslint|" and implement the parsing of program output.

The diagnsotic tool can also ensure that the foundry pipeline is advanced to the point it can be used to detect diagnostics.

\section{Interactive Tooltips}

In the early days of Builder, we would use a \verb|Gtk.Tooltip| to display diagnostic information when you hover over the problem area in the text editor.
That was problematic because sometimes you want to interact with the data inside that tooltip.
Something that cannot be done with \verb|Gtk.Tooltip|.

Eventually Builder got support for interactive tooltips in the form of a custom \verb|Gtk.Popover| that would dismiss when your mouse cursor left a tracking area.
That code as moved upstream into the \verb|GtkSourceView| library through the use of \verb|GtkSource.HoverProvider|.

Plug-ins may implement this interface to insert content into an interactive tooltip.
You may provide content in a number of formats or add \verb|Gtk.Widget| directly for more complex integration.

Users will be able to interact with the tooltip while it is displayed so long as the cursor does not leave the tracking area or the tooltip dismissed with \keystroke{Escape}, scrolling, or similar.

\begin{figure}[h]
\caption{An interactive tooltip displaying diagnostics}
\label{fig:interactive-tooltip}
\centering
\includegraphics[width=1\textwidth]{nodes/interactive-tooltip.pdf}
\end{figure}

\section{Code Actions}

"Code Actions" were popularized through the Language Server Protocol and are implemented in a number of language servers.
Builder provides \gidoc{iface}{CodeAction} which may apply a specific code action.

Generally, the language server protocol integration will create an instance of \gidoc{class}{LspCodeAction} which will call back into the language server to apply the action.

There is no reason that other plug-ins cannot do their own implementation of \gidoc{iface}{CodeAction} without a language server for things like refactoring or similar.

\section{Semantic Highlighting}

While \verb|GtkSourceView| provides the source code highlighting for Builder some languages benefit from semantic highlighting.
In this case we mean specific highlighting for features like class, function, and variable names.

Builder provides support for this using the \gidoc{iface}{Highlighter} interface.
The \gidoc{class}{Buffer} contains an \gidoc{class}{HighlightEngine} which performs incremental semantic highlighting using the configured \gidoc{iface}{Highlighter}.

In most cases, tooling that uses the semantic highlighter is fairly rudimentary.
For example, the \verb|clang| highlighter uses symbol names to semantic highlight.
That works well for classes and function names but less good at accurately handling variable names.

\begin{info}{}
Rather than improve \verb|Ide.Highlighter| it is expected that Builder would instead move towards something like \verb|treesitter| for semantic highlighting.
Either internally or through \verb|GtkSourceView|.
\end{info}

\section{Symbol Resolving}

Often times we need to know a little more about a piece of code than plain text.
The \gidoc{iface}{SymbolResolver} interface provides us a mechanism to lookup some basic information about the symbols in the file.

The editor uses this information to find out what function you are currently in while typing.
The \verb|symbol-tree| plug-in provides a tree of symbols from a popover in the \hyperref[section:statusbar]{status bar}.

You can jump to a declaration for what is under the cursor using this API.

You may also find references to a symbol using the \gidoc{iface}{SymbolResolver.find_references_async} method.
Use this when you are refactoring to find how API is being used.

\begin{info}{}
Note that some languages are fairly limited in what they support for finding references.
For example, the files may need to be open and indexed by the language server implementation to be discoverable.
\end{info}
