\chapter{Running Applications}
\label{chapter:running}

Builder uses the same \gidoc{class}{Runtime} from building to run the application as well.
Although when running \gidoc{vfunc}{Runtime.prepare_to_run} is used to prepare the \gidoc{class}{RunContext}.
This helps keep the modify—build—test—repeat cycle fast.

For containers this means setting up the \gidoc{class}{RunContext} to trampoline into the container with the right access and settings to display serviers, graphics drivers, network devices, etc.

\begin{info}{}
Builder takes the somewhat opinionated approach that you should always install the project before running it even if that is an alternative sysroot.
When developers create hacks for running "in-tree" it almost always eventually results in issues in production that should have been caught during development.
\\
\\
Developers may disable the installation requirement from preferences for cases where other mechanisms are in place to ensure the application is in working order.
\end{info}

\section{Flatpak Run Environment}

Some plug-ins such as \verb|flatpak| also provide additional features to ensure that running out of the build environment feels as if you're running an installed application.
Primarily this is because \verb|flatpak build|, which is used to run inside the target environment without an installation, does not provide numerous features normally associated with \verb|flatpak run|.

We do not use \verb|flatpak-builder --run| because that would require committing the application to an \verb|ostree| repository and performing cleanup operations.
These operations are destructive and would cause us to need to rebuild the application from the most recent \verb|cachepoint| which could be considerable extra work.

\section{The Run Manager}

The \gidoc{class}{RunManager} unsurprisingly manages the process of running an application.
It first needs to ensure that the project has been built and installed by requesting the \gidoc{class}{BuildManager} has advanced the current pipeline to at least the \verb|IDE_PIPELINE_PHASE_INSTALL| phase.

\subsection{Run Settings}

\begin{wrapfigure}{r}{0.3\textwidth}
\includegraphics[width=1\linewidth]{nodes/run-menu.pdf}
\label{fig:run-menu}
\end{wrapfigure}

Builder supports a number of tweaks that will be propagated when running the application.
You can use the "Run Menu" to set these tweaks and the \gidoc{class}{RunManager} will ensure the appropriate environment is setup when the application runs.

\section{Run Tools}

Builder uses the concept of an \gidoc{class}{RunTool} to abstract how an application will be run using tools like \verb|gdb| or \verb|valgrind|.
This allows the glue code for the tool to adjust command-line arguments so that they fit with the tools expectations.

Implementations of \gidoc{class}{RunTool} should make an effort to implement sending signals and forcing exit of the application using \gidoc{vfunc}{RunTool.send_signal}.
These operations are abstracted through \gidoc{class}{RunTool} to support situations where the application is running on a remote device.
As such there will be no way to send a signal locally and tool must handle that.

\subsection{Debugger}

Builder has abstractions dedicated to the operation of a debugger in the form of \gidoc{class}{Debugger}.
An \gidoc{class}{RunTool} exists specifically to bridge the appropriate \gidoc{class}{Debugger} into the mix.

Together this provides an easy way to integrate debuggers with the debugger panels as well as integrating with complex container environments such as \verb|podman| or \verb|flatpak|.

\begin{figure}[h]
\label{fig:debugger-panel}
\centering
\includegraphics[width=1\textwidth]{nodes/debugger-panel.pdf}
\end{figure}

The \verb|gdb| plug-in uses \verb|gdbwire| to communicate with a \verb|gdb| instance which is spawned as part of the \gidoc{class}{RunTool} integration.
See \gidoc{vfunc}{Debugger.prepare_for_run} for how to setup an \gidoc{class}{RunContext} layer to execute the appropriate debugger for your plug-in.

\subsection{Profiler}

The profiler in Builder is provided by Sysprof.
Much of the current Sysprof code was created to be part of Builder.
As such, it provides a \verb|sysprof-agent| which is installed in the \verb|org.gnome.Sdk|.
This provides a robust \gidoc{class}{RunTool} which can be controlled by Builder inside the container environment.

Sysprof is a sampling profiler which means it collects a sample of all code stacks on all CPUs at a regular interval.
Additionally it can collect all sorts of other counters, record message busses, track memory allocations, and more.

Builder will open the recorded profiling session with Sysprof when it has completed.
Previously Builder would embed the Sysprof user interface into an \gidoc{class}{Page} but that proved extremely difficult to maintain.

\begin{figure}[h]
\label{fig:sysprof}
\centering
\includegraphics[width=1\textwidth]{nodes/sysprof.pdf}
\end{figure}

\subsection{Valgrind}

Support for \verb|valgrind| is quite simple in implementation.
It will take the command to be executed and run it under \verb|valgrind| including any necessary command-line arguments.
A temporary file is used to contain the output of the \verb|valgrind| command and displayed in an \gidoc{class}{Page} once the application has exited.
