\chapter{SDKs and Runtimes}
\label{chapter:sdks}

Very early versions of Builder took, what was then, a radically new approach to IDE abstractions in that the container was front-and-center.
The abstraction was out of necessity to support \verb|Xdg-App| which is now known as \verb|Flatapk|.

All of the build infrastructure was routed through a sort of "execution abstraction".
For example, when configuring your project you don't want to configure from the environment that Builder is running.
Nor do you want to configure from the host environment in many cases.

Therefore \gidoc{class}{Runtime} provided a mechanism to execute build pipeline commands in the appropriate environment.

\section{Building vs Running}

The abstraction for executing commands in the build environment is also helpful for running the application.
However, there are caveats to be handled to ensure that applications run correctly.

The environment is usually different when running including the availability of \verb|WAYLAND_DISPLAY|, \verb|DISPLAY|, an accessibility bus, user installed fonts, and more.
The \gidoc{class}{Runtime} interface provides \gidoc{vfunc}{Runtime.prepare_to_build} to setup the build environment.
Use \gidoc{vfunc}{Runtime.prepare_to_run} to setup the environment for running applications.

It is essential to learn how \hyperref[chapter:run-context]{Run Contexts} work as they allow for rich layering of commands to cross container boundaries.

\section{SDK Providers}
\label{section:sdk-providers}

SDK providers are used, primarily, to allow users to install collections of software which will enable building applications.
Think of \gidoc{class}{Sdk} as an \gidoc{class}{Runtime} that can be installed and managed by the user.
We used to use \verb|Ide.Runtime| for this as well but the UI experience got overwhelming and begged for simpliciation.

\section{Runtime Providers}
\label{section:runtime-providers}

The add-in interface \gidoc{class}{RuntimeProvider} allows plug-ins to add and remove \gidoc{class}{Runtime} while Builder is running.
Notable examples of this are the \verb|flatpak|, \verb|jhbuild|, and \verb|podman| plug-ins.

The \verb|Flatpak| plug-in will detect changes to underlying installations and adjust the availability of \gidoc{class}{Runtime} in response.
Similarly, the \verb|Podman| plug-in will watch a number of JSON files in the user home directory for changes and ensure the available containers are represented in Builder.

\section{Flatpak}

The \verb|flatpak| plug-in provides the necessary bridge to allow Builder to build and run inside a Flatpak'd environment.
To do this, it "trampolines" through the host using the equivalent of \verb|flatpak-spawn --host| to run \verb|flatpak build| and setup the appropriate environment.

Builder must use the \verb|flatpak build| environment to also run applications because at no point is the application being developed a complete and installable Flatpak application.
To do that would require tramendous additional work every modify, build, test, repeat cycle.
Since a \verb|flatpak build| environment does not include an accessibility bus, Builder must bind in an appropriate accessibility bus for target application.
Additionally, user fonts are hoisted in using a similar mechanism as \verb|flatpak run| itself.

It is important that the Builder UI process stay lean in terms of dependencies because all code has bugs.
The less dependencies we pull into the UI process the better our chances at a resilient application developers can trust.

For that reason we do not link against \verb|libflatpak| in the UI process.
Instead there is a \verb|gnome-builder-flatpak| subprocess that we communicate with using point-to-point D-Bus protocol over a \verb|socketpair()|.
The daemon provides information about Flatpak installations, runtimes, SDKs, and SDK extensions.

\subsection{SDK Extensions}

One area where Flatpak has flourished is in making developer tooling available in the form of SDK extensions.
For example, the \verb|org.freedesktop.Sdk.Extension.llvm16| extension provides LLVM and Clang 16 for applications that need it.
The \verb|org.freedesktop.Sdk.Extension.rust-stable| similarly brings a stable rust toolchain and language server for applications and developer tooling to use.

If an application provides a Flatpak manifest builder will use that as the \gidoc{class}{Config} to build in the form of \verb|GbpFlatpakManifest|.
The configured Runtime, SDK, and any SDK extensions will be installed as a necessary preparation to setup the build pipeline.
