\chapter{Workbenches \& Workspaces}

Upon opening a project the user is displayed the \gidoc{class}{PrimaryWorkspace}.
There may only be a single \verb|Primary Workspace| opened for a project at a time.

There are other types of \gidoc{class}{Workspace} such as the \gidoc{class}{EditorWorkspace}.
If the user wants a second workspace window opened which is connected to the same project then the \verb|Editor Workspace| is used.

\begin{figure}[h]
\centering
\caption{The Primary Workspace}
\label{fig:primary-workspace}
\includegraphics[width=1\textwidth]{nodes/primary-workspace.pdf}
\end{figure}

Workspaces open within the same project are grouped together in a \verb|GtkWindowGroup|.
In Builder this is implemented as \gidoc{class}{Workbench}.
So there is a one-to-one-to-one relationship between a project, an \gidoc{class}{Context}, and an \gidoc{class}{Workbench}.

You can always find the \gidoc{class}{Workbench} from the \gidoc{class}{Context} using \gidoc{type_func}{Workbench.from_context}.

\begin{code}{}
IdeContext *context;
IdeWorkbench *workbench;

context = ide_object_get_context (IDE_OBJECT (my_object));
workbench = ide_workbench_from_context (context);
\end{code}

Operations like navigating to \gidoc{class}{Location} of files is done on the \gidoc{class}{Workbench} because it allows raising an existing \gidoc{class}{Page} instead of opening a new file.
See \gidoc{method}{Workbench.open_at_async} for opening files with their default \verb|content-type| handler.

\section{Workbench Add-ins}

When a \gidoc{class}{Workbench} is loaded it will in-turn load all of the \gidoc{iface}{WorkbenchAddin} from available plug-ins.

A \verb|Workbench Addin| is a great interface to implement when you want to extend functionally on a project-level.
For example you might want to use this interface if writing functionality that wants to save and restore session state for the project.

\subsection{Project Loading}

Not all workbenches contain a project.
For example, if using Builder in "editor-only" mode you'll have a \verb|Ide.Context|, \verb|Ide.Workbench|, but no project loaded.

To do special work when a project is loaded use the \gidoc{vfunc}{WorkbenchAddin.load_project_async} virtual method.
This is only called when there is an attempt to load a project and will only be called once.
To undo anything done when loading a project use \gidoc{vfunc}{WorkbenchAddin.unload_project_async} virtual method.

Sometimes you only want to enable some features if the user has opened a project.
In this case use the \gidoc{vfunc}{WorkbenchAddin.project_loaded} virtual method.

\subsection{Opening Files}

The \gidoc{class}{WorkbenchAddin} interface allows extending the "open file" mechanics of Builder.
For example, a plugin could look to see if the URI to be opened was of a certain scheme and handle that internally.

The editor plug-in handles this for common \verb|text/plain|-based file types.

See \gidoc{vfunc}{WorkbenchAddin.can_open} and \gidoc{vfunc}{WorkbenchAddin.open_async} for integrating into file opening mechanics.

\subsection{Session Management}

Developers spend a lot of time getting their environment just right.
So it is appreciated if when they come back to their project that things are where they left them.
Builder supports this through the concept of session state in \gidoc{class}{Session}.

Use the \gidoc{iface}{WorkbenchAddin} to implement \gidoc{vfunc}{WorkbenchAddin.save_session}.
Use \gidoc{vfunc}{WorkbenchAddin.restore_session} to restore the session state when the project is reloaded.

\subsection{Finding a Workbench Add-in}

Sometimes plug-ins have the need to communicate between various add-ins.
Other addins can find the \gidoc{class}{Workbench} using \gidoc{func}{widget_get_workbench} for any widget within the workbench.

Use \gidoc{method}{Workbench.addin_find_by_module_name} with the module name from your \verb|.plugin| file to get your workbench add-in instance.

\section{Workspace Add-ins}

When a \gidoc{class}{Workspace} is loaded it will in-turn load all of the \gidoc{iface}{WorkspaceAddin} from available plug-ins which match the appropriate workspace kind.
Use this interface if you want to extend the functionality of the primary or secondary workspace windows.

For example, if you want to add a new widget to the statusbar of a workspace use this add-in interface.
Use the \gidoc{vfunc}{WorkspaceAddin.load} virtual method to add your widget to the workspace.
Remove it in \gidoc{vfunc}{WorkspaceAddin.unload} so that it does not stay around if your plug-in is disabled.

Sometimes you need to change the visibility or property bindings based on what page is currently visible to the user.
See \gidoc{vfunc}{WorkspaceAddin.page_changed} to handle that.

Just like \gidoc{iface}{WorkbenchAddin} you may integrate with session state saving and restoring using \gidoc{vfunc}{WorkspaceAddin.save_session} and \gidoc{vfunc}{WorkspaceAddin.restore_session}.

There are many uses of \verb|Ide.WorkspaceAddin| within the Builder source code which can serve as more detailed examples.

\section{Header Bar}

\begin{figure}[h]
\centering
\caption{The Header Bar}
\label{fig:headerbar}
\includegraphics[width=1\textwidth]{nodes/headerbar.pdf}
\end{figure}

Use \gidoc{method}{Workspace.get_header_bar} to get the header bar for a workspace.
You can then add widgets to the edges using \gidoc{method}{HeaderBar.add}.
If you add a widget using an \gidoc{enum}{HeaderBarPosition} of \verb|LEFT_OF_CENTER| or \verb|RIGHT_OF_CENTER| they will be placed to the edge of the title, typically an \gidoc{class}{OmniBar}.

\subsection{The Build Menu}
\label{section:build-menu}

\begin{wrapfigure}{r}{0.33\textwidth}
\includegraphics[width=1\linewidth]{nodes/build-menu.pdf}
\label{fig:build-menu}
\end{wrapfigure}

Attached to the build button of the \gidoc{class}{OmniBar} is the build menu.
It provides various build-related actions along with the appropriate keyboard accelerator to activate them.
Many of these actions are available from the \hyperref[chapter:global-search]{Global Search} as well.

\section{Panels}

Builder provides robust support for movable widgets.
Originally part of Builder, the code was extracted into a new library \blink{https://gitlab.gnome.org/GNOME/libpanel}{Libpanel} which is used by other applications such as Zrythm and Sysprof.
Therefore changes to libpanel need to be done carefully to avoid breaking them.

There are three areas for panels including the left, right, and center bottom as seen in figure \ref{fig:panels}.
Widgets inside these panels must be an \gidoc{class}{Pane} and may be dragged between the different panel areas.

\begin{figure}[h]
\centering
\caption{Various panels displayed in the primary workspace}
\label{fig:panels}
\includegraphics[width=1\textwidth]{nodes/panels.pdf}
\end{figure}

It is common for plug-ins for developers to move panels around to be more ergonomic for their needs.
Therefore, common code in Builder's session management will try to place them in the same location when restoring common session state.

\section{Pages \& the Document Grid}
\label{section:the-grid}

Builder allows for \gidoc{class}{Page} to be added to an \gidoc{class}{Grid}.
However the implementation of the grid is mostly within \verb|libpanel|.

You can slice the grid in multiple directions, both columns and rows.
You need not have the same number of rows in each column.
Use \gidoc{signal}{Page.create-split} to create a new split of a widget when requested by the \gidoc{class}{Grid}.

\begin{figure}[h]
\centering
\caption{A grid of pages within Ide.Grid}
\label{fig:grid}
\includegraphics[width=1\textwidth]{nodes/grid.pdf}
\end{figure}

Each frame within the \gidoc{class}{Grid} is an \gidoc{class}{Frame}.
The frame widget handles changing between using a \verb|tabbar| and the historical dropdown widget of Builder.
Builder only supported the dropdown selector until the GTK 4 version was released out of code simplicity.
Users may enable the legacy dropdown selector using preferences and the \gidoc{class}{Frame} will adapt.

\begin{figure}[h]
\centering
\caption{The tabbar page selector of Ide.Frame}
\label{fig:tabbar}
\includegraphics[width=1\textwidth]{nodes/tabbar.pdf}
\end{figure}

The dropdown page selector will only show the current file at a time.
This was more useful when Builder packed many widgets into the page selector area.
Now that Builder contains a statusbar, this is less necessary and facilities the use of tab widgets.

\begin{figure}[h]
\centering
\caption{The dropdown page selector of Ide.Frame}
\label{fig:dropdown}
\includegraphics[width=1\textwidth]{nodes/dropdown.pdf}
\end{figure}

Pages may only be moved between other frames within the \gidoc{class}{Grid}.
They may not be moved to the left, right, or bottom panel areas.

\section{Status Bar}
\label{section:statusbar}

\begin{figure}[h]
\centering
\caption{The statusbar with an editor page open}
\label{fig:statusbar-editor}
\includegraphics[width=1\textwidth]{nodes/statusbar-editor.pdf}
\end{figure}

Most workspaces in Builder contain an \gidoc{class}{Statusbar} that is placed at the bottom of the document grid.
This allows the sidebar panels to have content from the top of the window to the bottom which creates a more pleasing asthetic.

Plug-ins tend to use \gidoc{iface}{WorkspaceAddin} to add widgets to the statusbar.
Use \gidoc{method}{Workspace.get_statusbar} to get the statusbar attached to a workspace.
It is very common for add-ins to implement \gidoc{vfunc}{WorkspaceAddin.page_changed} to update statusbar widget visibility based on the \gidoc{class}{Page} type.

\begin{figure}[h]
\centering
\caption{The statusbar with a documentation page open}
\label{fig:statusbar-docs}
\includegraphics[width=1\textwidth]{nodes/statusbar-docs.pdf}
\end{figure}

For example, in figure \ref{fig:statusbar-docs} the \verb|manuals| plug-in will display a "pathbar" to the current documentation if the focused page is a documentation page.
The \verb|editorui| plug-in does the same based on the current page being an \gidoc{class}{EditorPage}.

You could extend this functionality for web browser pages by displaying the URL of a link being hovered.
