\chapter{Build Systems}
\label{chapter:build-systems}

The \gidoc{iface}{BuildSystem} interface plays a critical role in the foundry support for a developer's project.
Many add-ins will use the build system object as a pivotal key into whether or not they should attach stages into the build pipeline.

Before you attempt to access the build system you should first check that the context supports projects.
Only a context which is opening a project will support the build system foundry.
See \gidoc{method}{Context.has_project} for more information.

Use \gidoc{type_func}{BuildSystem.from_context} to locate the build system from an \gidoc{class}{Context}.

\section{Build System Discovery}
\label{section:build-system-discovery}

For Builder to open a project it must first discover the build system in use.
This is performed by the \gidoc{iface}{BuildSystemDiscovery} add-in interface.
Many plug-ins use the helper subclass \gidoc{class}{SimpleBuildSystemDiscovery} to set a \verb|glob| which matches common path names and suffixes.

When the developer opens their project using an existing directory they have the option of specifying the primary build system file for the project.
For \verb|autotools| this might be their \verb|configure.ac| or \verb|configure.in|.
For \verb|meson| this might be their projects top-level \verb|meson.build|.

To allow for more specific control over the build system to use they may select it from the dropdown as seen in figure \ref{fig:open-project}.

The \verb|flatpak| plug-in will locate Flatpak manifests in \verb|*.json| files and peek at their contents.
This sometimes allows us to discover a build system more appropriate than what we discover in the root directory of a project.
If it has \verb|"build-system": "meson"| we might prefer Meson over autotools even though both build systems exist in-tree.

\begin{info}{}
It may be possible to make the \verb|Ide.BuildSystem| dynamic as the project changes configurations if that is deemed necessary in the future.
Historically the value would have been \verb|autotools| vs \verb|meson| for projects supporting both though that is increasingly uncommon.
\end{info}

\section{Extracting Build Flags}
\label{section:build-flags}

A critical role the \gidoc{iface}{BuildSystem} provides is the ability to introspect on the build system for compiler flags.
For example, tooling for compilers like C, C++, Objective-C, and Vala all need compiler flags to be able to parse project sources.
Parsing sources is how features like diagnostics and code completion are built so access to this information is vital.

Many build systems will now provide a \verb|compile_commands.json| file which describes how a particular file is compiled.
It is suggested that those writing build systems extract the information using this mechanism.
The \verb|meson| plug-in does just that although performs a number of post-processing steps to improve the quality of results.

See \gidoc{class}{CompileCommands} for a helper object which can parse and extract information from \verb|compile_commands.json| files.

This should all be resolved using a virtual method like \gidoc{vfunc}{BuildSystem.get_build_flags_for_files_async} for a batch of input files at once.
Failure to support batched lookup will often slow down features like code-indexing considerably as they will hammer this API for information.

\subsection{Build Flags from Autotools}

Extracting build flags from \verb|autotools| is a sort of mind-melting adventure.
I was once told by a dear friend that “the only way to parse make is with make.”
So when we want to extract build flags from \verb|autotools| projects we run \verb|make| and sent it a command on \verb|stdin| which looks like the following.

\begin{code}{}
include Makefile
print-%: ; @echo $* = $($*)
\end{code}

The result is that we can get a convenient \verb|key = value| format on \verb|stdout| which is easy enough to parse.

Given that compilers are often tricky things in-and-of-themselves, we fake our \verb|$CC| and other common environment variables so we know where the build flags start in a complex managerie of command-line arguments.

A dry run of the build system is then cached in a file we call the \verb|makecache| to accelerate parsing of build flags without requiring the execution of a \verb|make| subprocess.

\section{Preparing Tooling}
\label{section:prepare-tooling}

A significant amount of plug-ins in Builder need to execute some code inside the build system environment.
The build system may supplement an \gidoc{class}{Runtime} by further configuring the environment for command-line tools such as linters, language servers, code-formatters, and more.

Use \gidoc{iface}{BuildSystem.prepare_tooling} to supplement the execution environment.
You'll find plug-ins like \verb|meson| use this in combination with \verb|meson devenv| to more accurately represent the environment a build would use.

It is essential to understand how \gidoc{class}{RunContext} works to be precise in applying the environment at the right layer.

\section{The Build Manager}
\label{section:build-manager}

The \gidoc{class}{BuildManager} is responsible for setting up build pipelines and running them and is shared across all project types.
It will track changes to the selected \gidoc{class}{Config} to invalidate old pipelines and create new ones.

Use \gidoc{type_func}{BuildManager.from_context} to access the build manager for an \gidoc{class}{Context}.
You should first check if the context supports projects by calling \gidoc{method}{Context.has_project}.

It tracks whether a pipeline is currently running and provides stateful actions to start, stop, clean, and rebuild a pipeline.
These actions are attached to the \gidoc{class}{Workbench} for easy access from \verb|Gtk.Actionable| widgetry.
For example, the \verb|context.build-manager.build| action within an \gidoc{class}{Workspace} would activate the \verb|build| action of the \gidoc{class}{BuildManager}.

The build manager is also responsible for the initial prepartion for a pipeline.
Setting up the pipeline may require installing dependencies such as SDKs.
It also might require toolchain initialization or even waiting for a Qemu device to become available.

The \gidoc{class}{BuildManager} also keeps some metadata about a build such as error and warning counts.

See the \hyperref[section:build-menu]{build menu} for how some of these actions can be activated.

\section{Build Pipelines}
\label{section:pipelines}

An \gidoc{class}{Pipeline} consists of a series of \gidoc{class}{PipelineStage} grouped together by their respective \gidoc{flags}{PipelinePhase}.
These stages are typically registered by \gidoc{iface}{PipelineAddin} based on the \gidoc{property}{ConfigManager.current} configuration, \gidoc{class}{BuildSystem}, or \gidoc{class}{Runtime}.

A stage is registered to a single \gidoc{flags}{PipelinePhase} in the pipeline.
For example, the \verb|meson| plug-in would register a stage which runs \verb|ninja| as part of the \verb|IDE_PIPELINE_PHASE_BUILD| phase.

A build pipeline has a target \gidoc{class}{Device} which allows it to self-configure for the appropriate architecture type which may be different than the host system.
This is useful in situations where an \verb|x86_64| system wants to build and run an \verb|aarch64| binary using \verb|qemu|.
The default device represents the host system.

Additionally a pipeline is configured with an \gidoc{class}{Runtime} based on the \gidoc{class}{Config}.
Stages that run commands are expected to utilize this runtime as the environment for executables.
Using \gidoc{class}{PipelineStageCommand} is a convenient way to do that while focusing on a simpler \gidoc{class}{RunCommand} instead.

\subsection{Pipeline Phases}

The pipeline phases consist of the following and always progress from \verb|prepare| towards \verb|export|.

\begin{description}
\item[Prepare] Early init, creating directories, etc
\item[Downloads] Ensure remote resources are cached locally
\item[Dependencies] Build dependencies required before configuring
\item[Autogen] Bootstrap build system if necessary (\verb|autogen.sh|)
\item[Configure] Configure build system if necessary (\verb|meson setup|)
\item[Build] Perform the build (\verb|make| or \verb|ninja|)
\item[Install] Install to configured location (\verb|make install|)
\item[Commit] Commit the build artifacts for export (\verb|ostree commit|)
\item[Export] Create a package from committed artifacts (\verb|Flatpak Bundle|)
\end{description}

Builder will skip stages within those phases which are marked as complete.
Only a single stage may run at a time though it would be possible for a plug-in to write a \verb|multi-stage| which can execute multiple sub-stages in concurrently.

To avoid doing all work on every build request Builder only executes stages up to the requested phase.
Pressing the “Build” button would request the pipeline advance to the \verb|build| phase.

You may also attach stages to phases using either \verb|IDE_BUILD_PHASE_BEFORE| or \verb|IDE_BUILD_PHASE_AFTER| flags to specify they run before or after a phase of the pipeline has reached.

\subsection{Skipping Work with Queries}

An \gidoc{class}{PipelineStage} may skip work by connecting to the \gidoc{signal}{PipelineStage.query} signal.
Use this signal to check if your stage really needs to be run.
If not, there may be a chance you can reduce time for user builds.

For example, the \verb|meson| plug-in will try to avoid running \verb|meson setup| if it detects the project is already configured.

It is best to do this work asynchronously when you need to check the contents of data on disk or other system with risk of latency.
While performing that asynchronous work use \gidoc{method}{PipelineStage.pause}.
When the asynchronous work has completed use \gidoc{method}{PipelineStage.unpause} to resume the pipeline.

\subsection{Chaining Stages}

Some build systems perform similar work for cooperative commands.
For example, \verb|make| followed by \verb|make install| could be reduced to just \verb|make install|.
The \gidoc{vfunc}{PipelineStage.chain} allows adjacently run stages to elide extraneous work.

\subsection{Stage Cleanup}

Sometimes the developer wants to clean their build root similar to a \verb|make distclean|.
The \gidoc{signal}{PipelineStage.reap} signal allows a stage to provide files or directories they would like removed.
The \gidoc{class}{DirectoryReaper} will take care of that work for them before the pipeline is advanced again.

Alternatively you can implement \gidoc{vfunc}{PipelineStage.clean_async} to manage your own cleanup.
Some plug-ins use this feature with an \gidoc{class}{RunCommand} executed for cleanup.

\subsection{Cancellation}

The developer should be in ultimate control of what is happening on their computer.
It is expected that \gidoc{class}{PipelineStage} properly handle \verb|Gio.Cancellable| provided to their API.
This ensures that when the developer presses the "stop building" button that inflight tasks are cancelled, stopped, or otherwise left inert.

If you are working with \verb|libdex| it can be handy to await on a combined future of the \verb|cancellable| and whatever external future needing completion.

\begin{code}{}
dex_await (dex_future_first (dex_cancellable_new_from_cancellable (cancellable),
                             some_other_future (),
                             NULL),
           NULL);
\end{code}

\subsection{Build Pipeline Add-ins}
\label{section:pipeline-addins}

Use the \gidoc{iface}{PipelineAddin} interface to register your own pipeline stages.
Well behaved add-ins will undo anything done in \gidoc{vfunc}{PipelineAddin.load} in \gidoc{vfunc}{PipelineAddin.unload}.
One easy way to do that is to allow the pipeline addin to "track" your known stage-ids with \gidoc{method}{PipelineAddin.track}.
They will automatically be removed when unloading the add-in.

Use \gidoc{method}{Pipeline.attach} to attach your stage.
If you have an \gidoc{class}{RunCommand} you can use the helper \gidoc{method}{Pipeline.attach_command} instead of creating your own stage.

\begin{info}{}
It is best to avoid using \gidoc{class}{PipelineStageLauncher} or \gidoc{class}{SubprocessLauncher} directly.
They existed before \gidoc{class}{RunContext} and therefore are easy to get wrong in terms of propagating the appropriate environment.
\end{info}

It is wise to check pre-conditions before registering your pipeline stages.
For example, if your stage only affects \verb|flatpak| builds then check that the \gidoc{class}{Config} of the pipeline is a \verb|GbpFlatpakManifest| when your add-in loads.
If not short-circuit so you do not create additional stages which could cause issues with builds.


