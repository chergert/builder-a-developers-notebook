\chapter{Introduction}

This document is meant to be a high-level guide to how GNOME Builder is assembled.
It should help you see how the components are assembled and the relations between them.

It is my hope that this helps both potential contributors understand the codebase and other developers learn from our mistakes as to how to build a framework for large GTK-based applications.

\section{Builder, The Project}

The Builder project is developed with the intention that it is the premier development tool to develop for the GNOME desktop.
As such, the development happens \href{https://gitlab.gnome.org/GNOME/gnome-builder}{on GNOME's gitlab instance}.

Engineering defects are tracked as issues within the \href{https://gitlab.gnome.org/GNOME/gnome-builder/issues}{issue tracker}.
Some minimal effort to tag them is done once we've triaged the issue so that we can find common issues within a subsystem.

As of writing we prefer to keep feature requests out of the issue tracker.
The primary reason is that feature requests are often incomplete or competing with the stated goals of the project.
We prefer to jot down information about users desired features and instead track them externally so that some curation and refinement may occur.
Once an accepted goal is determined, then designed, we can file an issue to track implementation.

\section{Principles}

Builder is a very principled project.

Chief among them is that if we can't do it fast, don't do it.
It's important that we figure out how to write a feature in a way that is both useful and efficient before we push that out to developers.

We also focus on simplicity of design.
That might mean we move slower than our competition but hopefully it results in a better project when we get there.

We also try to ensure that we are developing and improving our upstreams.
That means that we must do a lot of work on GTK, Pango, GObject and more as part of our project.
We do not shy away from fixing the root of a problem rather than paper over it.

We want to set an example for developers in our ecosystem.
If they're using Builder to write their applications, it's natural that behaviors of Builder will get mimiced.
So make sure there is quality to mimic.

The details are extremely important for ergonomics so take the time to round the rough edges.

And most importantly, keep a youthful spirit to exploring and implementing new features.
There is so much that can be done which we haven't even begun to implement.

\section{Documentation}

You can find the API reference for Builder at \href{https://devsuite.app/docs/libide/}{https://devsuite.app/docs/libide/}.
This is useful for both people contributing to Builder internals as well as people writing plugins for Builder.

User documentation is provided at \href{https://devsuite.app/help/gnome-builder/}{https://devsuite.app/help/gnome-builder/}.

\section{Licensing}

Builder is licensed under the GNU GPLv3-or-newer.
All code that is linked or pulled into Builder must be compatible with that license.

\section{Releases}

We release Builder in coordination with the GNOME release cycle.
That includes alphas, betas, release-candidates, and then finally stable releases.
This cycle covers a 6 month span and slightly overlaps in terms of minor releases after the \verb|.0|.

Our continuous integration pipelines create a "nightly" Flatpak release of Builder after every source code commit.
These releases are published to \verb|nightly.gnome.org| immediately afterwards.

The nightly releases of Builder have quite a few users and developers from GNOME using them so we try to keep them working and relatively stable.

\section{Portability}

Builder officially supports GNOME on Linux.
And specifically when distributed using Flatpak.

However, we are not obtuse about it.
When people show up with patches for FreeBSD and other operating systems we should do our best to integrate them.

We support a recent GCC compiler release for Builder.
However, we are happy to take fixes that improve Builder with other compilers.
If you want to distribute Builder using another compiler, such as Clang, you may need to fix things up from time to time.

Generally our watermark for what we support is whatever is in the correlated \verb|org.gnome.Sdk| release for that version of GNOME.

\section{Dependencies}

Create Builder necessitated creating a number of other libraries and tooling.
Much started out inside of Builder and then was extracted out to make it easier to test and maintain.

Working on Builder itself may lend you to regularly file bugs and triage bugs in any number of these projects.

\subsection{GObject, GTK, \& Adwaita}

We always depend on the newest releases of \verb|GObject|, \verb|GTK|, and \verb|libadwaita|.
It is common that we find issues in these libraries as we push the boundaries of features.

\subsection{Libpanel}

Builder originally contained all of the code for managing panels and grids of pages.
However other applications find that code to be useful so it was extracted into \verb|libpanel|.
It provides the base for workspaces within Builder.

\subsection{GtkSourceView}

The editor is powered by the \verb|GtkSourceView| project.
It is a sister project of Builder as many of Builder's features were pushed upwards into GtkSourceView.

Features like syntax highlighting, code completion, snippets, VIM-emulation, and color schemes live within this project.

Builder extends color schemes provided by GtkSourceView to restyle the entire Builder application windows.

We use the newest GtkSourceView release as they are released in tandom with Builder and Text Editor.

\subsection{Libpeas}

A sister project of Builder is \verb|libpeas|.
It provides infrastructure for managing and implementing plug-ins.

Since most of Builder is implemented using plug-ins it is extremely important to understand how it works.

Builder used to use \verb|PyGObject| as a plug-in scripting language.
However, for stability reasons we moved to \verb|GJS| and implemented support for that upstream in \verb|libpeas|.
Plug-ins bundled with Builder, however, are all written in C.

\subsection{VTE}

Nearly all of the terminal capabilities in Builder are performed by VTE.
We make heavy use of \verb|Vte.Terminal| and \verb|Vte.Pty|.
A "PTY" is a psuedo-terminal device in the Linux kernel (or similar OS) which is used to create the devices used for \verb|stdin|, \verb|stdout|, and \verb|stderr| of processes.

\subsection{Flatpak}

Builder is not only distributed with Flatpak but uses it heavily to provide IDE features.
For example, Flatpak provides many common SDKs when developing applications with Builder.

Noteworthy though is that we do not link \verb|libflatpak| in the main application process.
We use a \verb|gnome-builder-flatpak| subprocess which is connected via a \verb|socketpair()| using point-to-point D-Bus protocol.
This helps protect the application by isolating \verb|libflatpak|.

\subsection{WebKit}

Web browser features are provided by the GTK port of WebKit.
It is an important part of our GTK stack and undoubtedly the most complex.
We occassionally need to adapt our use of it to take advantage of new features like sandboxing.

\subsection{Libsoup}

We use \verb|libsoup| for HTTP related download operations.
It is important that the version we use match what is used by \verb|WebKit|.

\subsection{Json-GLib \& Jsonrpc-GLib}

Json-GLib provides general JSON parsing for Builder.
This gets used in things like parsing Flatpak manifests, Podman configurations, and more.

Building up on that is Jsonrpc-GLib which was extracted from Builder.
It provides the necessary abstractions for communicating with services using the \blink{https://langserver.org/}{Language Server Protocol}.

\subsection{Template-GLib}

Another project extracted from Builder is Template-GLib.
It provides template expansion using injected state variables and evaluating a light scripting language within the template.
Those scripts may call back into Builder or other libraries using \verb|GObject Introspection|.

We also build upon the template script language for keyboard shortcuts.
It allows us to implement \verb|keybindings.json| to make keyboard shortcuts activatable based on application state.

\subsection{editorconfig}

To parse \verb|.editorconfig| files we use the \verb|editorconfig| library.
We provide some wrappers around it to make it easier to consume from various abstraction layers.

The \verb|.editorconfig| format is just different enough from what \verb|GLib.KeyFile| uses that it makes sense to use the external library instead.

\subsection{Enchant}

Builder includes support for spellchecking and dictionaries are provided by \verb|enchant2|.
Eventually that code was extracted from Builder (and Text Editor) into \verb|libspelling|.
However, Builder has not yet adapted to using the external library.

\subsection{Libdex}

Builder contains a lot of asynchronous code which has multiple phases.
Writing code and managing state across those calls can be extremely tedious and error prone.

New code in Builder should be using \verb|libdex| and \verb|DexFuture| to manage asynchronous state.
If complex enough, it should also use \verb|dex_scheduler_spawn()| to create \verb|DexFiber| and write synchronous looking code which is in-fact asynchronous.

\subsection{Gom \& SQLite}

Gom (GObject Object Mapper) provides an abstraction over SQLite which can automatically save and load \verb|GObject| to and from SQLite storage.

It is used to store information like documentation for fast search and cross-referencing.

\subsection{libgit2 \& libgit2-glib}

Builder provides some light Git capabilities.
\verb|libgit2| and \verb|libgit2-glib| implement many tricky features and so we isolate it from Builder for safety and stability concerns.

Builder only links to them from the \verb|gnome-builder-git| subprocess which communicates with the application process using a \verb|socketpair()| using D-Bus point-to-point serialization.

\subsection{Clang \& LLVM}

Builder provides a \verb|libclang| based language server in \verb|gnome-builder-clang|.
It is preferred over \verb|clangd| because it integrates better with project information in Builder.
Additionally, it tends to use less memory while having similar feature sets.

Additionally, \verb|gnome-builder-clang| uses \verb|jsonrpc-glib| which self-optimizes using a more efficient protocol than JSONRPC when jsonrpc-glib is on the other side of the connection.

If things were to change, we can switch to using \verb|clangd|.

\verb|Libclang| tends to crash so we isolate it from the Builder application process for safety reasons.

\subsection{Sysprof}

Sysprof is a sister project to Builder.

Builder used to link Sysprof into the application to provide viewing capabilities of profiler data.
However, that became too complex to manage.

So no, Builder opens capture files with Sysprof directly as an external process.
Builder still links to \verb|sysprof-capture| so that it can annotate profiler data when Builder itself is profiled.

\subsection{Libxml2}

XML editing capabilities are provided using \verb|libxml2|.
Tthere are safety concerns about doing this long term and at some point we would like to extract XML capabilities into either an XML language server or similar.

What holds that back currently is that we do some additional work around supporting \verb|*.ui| files with GTK.
