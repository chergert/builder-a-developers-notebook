From the very earlier prototypes of Builder it was designed with the intention to be multi-process.
I wanted the same sort of resiliency benefits that Google Chrome had seen by breaking the application into more processes.

Initially this was done by creating an \verb|Ide.Worker| which would initialize a \verb|gnome-builder| subprocess running as the worker.
It could host just the single plug-in we wanted isolated and that worked pretty well using the venerable \verb|Gio.DBusConnection| over a private \verb|socketpair()|.

However, after Builder was released to the world a new design spread through the developer tooling industry in the likes of Language Server Protocol.
It is this author’s belief that the protocol is extremely poorly designed and fails on some of the most fundamental merits of performance, multi-process coordination, and managing version skew.
Despite that, you cannot ignore how successful its been at improving the quality of tooling integration.

Supporting LSP these days is a must and the following chapters will discuss how it is implemented.

\chapter{Ide.LspService}
\label{section:lsp-service}

The \gidoc{class}{LspService} provides an \gidoc{class}{Object} on the object graph which manages a subprocesses implementing the language server protocol.

What context the subprocess runs is of great importance because it affects what the language server may see.
For example, you likely want to run the language server in the build pipeline because that is where the build dependencies and source code are available.
If that isn’t possible you may be able to run it on the host, albeit with different dependencies loaded in.

If the language server exits or otherwise crashes an \gidoc{class}{SubprocessSupervisor} will make sure it is respawned.

The service will ensure that the \gidoc{class}{LspClient} is setup to communicate over the \verb|pipe(2)| connected to the language server once the process has spawned.

\chapter{Ide.LspClient}
\label{section:lsp-client}

Generally, to communicate with a language server \verb|stdin| and \verb|stdout| are used.
This can cause problems with stray \verb|printf()| found in language server code or tooling libraries as it will break the communication protocol.

The client manages the core communications channel to the language server.
That communication happens in the form of \verb|JSON-RPC| which is handled by \verb|Jsonrpc.Client| from the \verb|jsonrpc-glib| library.
Originally that code existed in Builder but was extracted to help other projects including other GTK-based IDEs.

Most add-in types around LSP integration will submit commands and process replies from the \gidoc{class}{LspClient}.

\section{Debugging JSON-RPC Issues}

You can see the communication on the wire in logging output at debug level if you set the \verb|JSONRPC_DEBUG=1| environment variable.

\begin{shell}{}
$ flatpak run --env=JSONRPC_DEBUG=1 org.gnome.Builder.Devel -vvv
\end{shell}

You can also see when a language server has spawned or exited in the messages panel.

\begin{figure}[h]
\caption{The messages panel contains useful troubleshooting information}
\label{fig:messages}
\centering
\includegraphics[width=1\textwidth]{nodes/messages.pdf}
\end{figure}

\section{Lazy Setup}

It is important that creating an add-in instance alone doesn't cause a build to start on the user's behalf.

This could happen if the add-in requests the client object, which in turn needs to be executed within the user's build pipeline.
But you can't execute anything in the build pipeline until it is setup and advanced to the configure phase.
To reach the configure phase you may need to build project dependencies.

To prevent such a situation various add-ins can do "lazy binding" to the client.
That means they themselves won't trigger a language server to start but will take advantage of language server once it does start.

\chapter{Language Server Add-ins}

\section{Ide.LspCompletionProvider}

This add-in implementation provides the \verb|GtkSource.CompletionProvider| interface bridged to the language server through \verb|textDocument/completion|.
Individual completion proposals are of type \gidoc{class}{LspCompletionItem}.

In most cases you'll get an \verb|Ide.LspPluginCompletionProvider| when implementing LSP plug-ins using the simplified \verb|.plugin| format as seen when \hyperref[section:new-lsp]{Adding a new Language Server}.

\section{Ide.LspDiagnosticProvider}

This add-in implementation provides the \gidoc{iface}{DiagnosticProvider} interface bridged to the language server via "published-diagnostics" notifications.
Individual diagnostics are of type \gidoc{class}{LspDiagnostic}.

In most cases you'll get an \verb|Ide.LspPluginDiagnosticProvider| when implementing LSP plug-ins using the simplified \verb|.plugin| format as seen when \hyperref[section:new-lsp]{Adding a new Language Server}.

\section{Ide.LspFormatter}

This add-in implementation provides the \gidoc{iface}{Formatter} interface bridged to the language server via \verb|textDocument/formatting|.
It also supports formatting entire documents and ranges when the language server also supports it.

In most cases you'll get an \verb|Ide.LspPluginFormatter| when implementing LSP plug-ins using the simplified \verb|.plugin| format as seen when \hyperref[section:new-lsp]{Adding a new Language Server}.

\section{Ide.LspHighlighter}

This add-in provides an \gidoc{iface}{Highlighter} for semantic highlighting which is backed by symbol information provided from the language server.
It would be nice if future revisions of the LSP provided for more robust support here but things will likely move towards using something like \verb|treesitter| instead.

\section{Ide.LspHoverProvider}

This add-in implementation provides the \verb|GtkSource.HoverProvider| interface bridged to the language server via \verb|textDocument/hover|.
The format returned can be in various formats which will be displayed using the appropriate widget by a \verb|GtkSourceView| interactive tooltip.

In most cases you'll get an \verb|Ide.LspPluginHoverProvider| when implementing LSP plug-ins using the simplified \verb|.plugin| format as seen when \hyperref[section:new-lsp]{Adding a new Language Server}.

\section{Ide.LspRenameProvider}

This add-in provides an \gidoc{iface}{RenameProvider} for light refactoring bridged to the language server via \verb|textDocument/rename|.

In most cases you'll get an \verb|Ide.LspPluginRenameProvider| when implementing LSP plug-ins using the simplified \verb|.plugin| format as seen when \hyperref[section:new-lsp]{Adding a new Language Server}.

\section{Ide.LspSymbolResolver}

This add-in provides an \gidoc{iface}{SymbolResolver} for extracting symbol information about a file.
It uses various RPC such as \verb|textDocument/documentSymbol|, \verb|textDocument/definition|, and \verb|textDocument/references|.

In most cases you'll get an \verb|Ide.LspPluginSymbolResolver| when implementing LSP plug-ins using the simplified \verb|.plugin| format as seen when \hyperref[section:new-lsp]{Adding a new Language Server}.

\section{Ide.LspSearchProvider}

The search provider add-in uses a list of workspace symbols from the language server to allow the user to jump to them via global search.
The RPC used specifically is \verb|workspace/symbol|.

\section{Ide.LspCodeActionProvider}

This add-in implementation provides the \gidoc{iface}{CodeActionProvider} interface bridged to the language server via \verb|textDocument/codeAction|.

In most cases you'll get an \verb|Ide.LspPluginCodeActionProvider| when implementing LSP plug-ins using the simplified \verb|.plugin| format as seen when \hyperref[section:new-lsp]{Adding a new Language Server}.

\chapter{Plug-ins}

Language server integration in Builder original came in the form of plugins written in C which implemented the supported add-ins.
That turned out to be a lot of duplicated code and rather cumbersome to add new plugins unless you knew C.
Since people wanting their favorite Language Server integrated tend to not know C, it makes sense to add LSP support without using too much C code.

Builer 44 introduced support for language servers through the use of a \verb|.plugin| without needing C code.

\section{Supported Language Servers}

\begin{multicols}{2}
\begin{itemize}
\item Bash
\item Blueprint
\item Clang (C, C++, and Objective-C)
\item D-lang
\item Elixir
\item Go
\item Java
\item JavaScript
\item Lua
\item PHP
\item Python
\item Rust
\item Swift
\item TypeScript
\item Vala
\item Zig
\end{itemize}
\end{multicols}

\section{Adding a new Language Server}
\label{section:new-lsp}

The easiest way to get going is to copy another language server plugin.
You can mostly do that and rename the files to match your LSP of choice.

Don't forget to add an option to \verb|meson_options.txt| for the plugin.
Additionally you'll want to add a \verb|subdir()| to \verb|src/plugins/meson.build| for your plugin.

Usually the plugin option should be named \verb|'plugin_mylsp'| where \verb|mylsp| is replaced with the name of your language server.

\begin{code}{src/plugins/ts-language-server/ts-language-server.plugin}
[Plugin]
Builtin=true

# The name of the module (where it is in /plugins)
Module=ts-language-server

# General plugin Information
Name=Typescript Language Server
Description=Language Server for JavaScript/TypeScript
Copyright=2024 You

# Categorizes within Preferences
X-Category=lsps

# This signifies the "no code" LSP plugin
Embedded=ide_lsp_plugin_register_types

# What command to execute
X-LSP-Command=typescript-language-server --stdio

# An embedded settings json file which is used to
# initialize the peer if necessary.
X-LSP-Settings=settings.json

# This tells various Addin providers what languages the
# LSP plugin supports.
X-Code-Action-Languages=js,typescript
X-Completion-Provider-Languages=js,typescript
X-Diagnostic-Provider-Languages=js,typescript
X-Formatter-Languages=js,typescript
X-Highlighter-Languages=js,typescript
X-Hover-Provider-Languages=js,typescript
X-LSP-Languages=js;typescript;
X-Rename-Provider-Languages=js,typescript
X-Symbol-Resolver-Languages=js,typescript
\end{code}

Take a look through a number of other language server plug-ins to create one for your favorite LSP we might be missing.

