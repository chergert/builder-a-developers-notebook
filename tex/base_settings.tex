\chapter{User Settings}

Users often have an expectation with IDEs that settings allow for per-project overrides.
That can be difficult when using \verb|GSettings| because it does not have built-in facilities to provide this.

To simplify settings with overrides \verb|libide-core| contains an \gidoc{class}{Settings} object which does much of what the \verb|GSettings| object can do.
However, it allows stacking multiple \verb|GSettings| paths so that overrides are applied as necessary.
The first layer with a user-specified change will take precedence.
If no change is made, the settings default value is used.

In practice this means that you have the same relocatable \verb|GSettingsSchema| with values at different paths.
The first path with a change wins. Per-project overrides tend to be placed at "/org/gnome/builder/projects/\verb|project_id|/" or some suffix there-of.

You can find more information about how users can alter application-wide and per-project settings in \hyperref[chapter:preferences]{Preferences \& Tweaks} where the UI is covered in more detail.

\section{Plug-ins}

If you are writing a plugin you should put your \verb|.gschema.xml| file within the same directory as your plugin.

The settings schema need not provide the \verb|path=""| if using \gidoc{class}{Settings} as the path will be dynamically generated for both application-wide and per-project overrides. See the following example which also uses \verb|gnome-builder| as the translation domain for GNOME translators to provide translations.

\begin{code}{org.gnome.builder.myplugin.gschema.xml}
<?xml version="1.0" encoding="UTF-8"?>
<schemalist>
  <schema id="org.gnome.builder.myplugin"
          gettext-domain="gnome-builder">
    <key type="b" name="my_option">
      <default>false</default>
    </key>
  </schema>
</schemalist>
\end{code}

You can ensure the schema is installed by adding the following to your plug-in's \verb|meson.build|.

\begin{code}{Installing the GSettingsSchema}
install_data(['org.gnome.builder.myplugin.gschema.xml'],
             install_dir: schema_dir)
\end{code}

\section{Action Integration}

\gidoc{class}{Settings} implements \verb|Gio.ActionGroup| natively and can be inserted into the \verb|Gtk.Widget| hierarchy with \verb|gtk_widget_insert_action_group|.

Use \gidoc{class}{GSettingsActionGroup} to expose \verb|Gio.Settings| directly as \verb|Gio.ActionGroup|.
This can save you a lot of code binding settings values to actions or properties of objects.

Use \gidoc{class}{SettingsFlagAction} to expose a single flag of a \verb|Gio.Settings| bitfield as an action.
