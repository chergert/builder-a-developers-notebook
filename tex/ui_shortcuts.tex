\chapter{Keyboard Shortcuts}
\label{chapter:shortcuts}

Shortcuts are managed via the private type \verb|Ide.ShortcutManager| in \verb|libide-gui|.
This object is a \verb|Gio.ListModel| of \verb|Gtk.Shortcut| which contains all of the possible shortcuts that may be executed.

To say the \verb|Ide.ShortcutManager| is a \verb|Gio.ListModel| glosses over the design a slight bit.
In reality it wraps a \verb|Gtk.FlattenListModel| of many \verb|Gio.ListModel| to create the amalgamation list.

They get attached to a toplevel such as \gidoc{class}{PrimaryWorkspace} through the use of a \verb|Gtk.ShortcutController|.
We attach two controllers so that shortcuts may activate in either capture or bubble event dispatch phases based on their needs.
The models attached to the controllers are pre-filtered for their appropriate propagation phase.

\section{Shortcut Providers}

Plug-ins can use \gidoc{iface}{ShortcutProvider} for ultimate control over how their \verb|Gtk.Shortcut| are integrated into the \verb|Ide.ShortcutManager|.

Implement \gidoc{iface}{ShortcutManager.list_shortcuts} to provide a \verb|Gio.ListModel| that updates as necessary.

\section{Shortcut Bundles}

Shortcuts can be a fairly tricky thing because you don't want them to activate in all situations.
To help with this, Builder provides a \verb|keybindings.json| file which allows for defining shortcuts as well as \verb|when| they are activatable.

The \verb|when| field of a shortcut may contain a \verb|template-glib| expression which is evaluated to determine availability.
First the shortcut trigger must match what is being activated.
Afterwards, if the \verb|when| expression evaluates to \verb|false| then it will be skipped and the controller will continue to locate the next match.

To make writing expressions easier \verb|libide-gui| contains a \verb|keybindings.gsl| of utility functions.
Note that \verb|template-glib| allows calling into \verb|GObject Introspection| so you can check various workbench, workspace, or page state.

To provide a shortcut bundle from your plug-in simply provide a \verb|gtk/keybindings.json| file as a resource in your plug-in's resources.

\begin{code}{keybindings.json}
{
  /* If you provide an "id" they shortcut can be
   * changed by the user in preferences. It will
   * also update the "Shortcuts Dialog" and menu
   * accelerator labels.
   */
  "id" : "org.gnome.builder.workspace.goto-page-1",

  /* The trigger causing activation */
  "trigger" : "<Alt>1",

  /* A GAction to activate */
  "action" : "frame.page",

  /* GVariant string format of args for @action */
  "args" : "1",

  /* An expression to evaluate, defaults to true */
  "when" : "inGrid()",

  /* The short controller phase this runs in */
  "phase" : "capture"
},
\end{code}

\section{User Overrides}

\begin{figure}[h]
\caption{Builder keyboard shortcuts, overridable by the user}
\label{fig:keybindings}
\centering
\includegraphics[width=.85\textwidth]{nodes/shortcuts.pdf}
\end{figure}

Users sometimes have their own preferences for keyboard shortcuts.
Builder attempts to accomodate this with user overrides which can be tweaked in \hyperref[chapter:preferences]{Applicatin Preferences}.

The overrides work very similar to the \verb|keybindings.json| files that plug-ins may provide except they also contain an \verb|"override": "id"| field.
Builder stores the user overrides in \verb|.config/gnome-builder/keybindings.json| or equivalent when used from Flatpak.

The preferences dialog in Builder provides access to the list of keyboard shortcuts, their descriptions, and ability to override them.
Alternatively, the user may edit the \verb|keybindings.json| file directly.

\section{Shortcut Scripts}

As mentioned previously, Builder provides a \verb|keybindings.gsl| file with helpful functions for keybindings.
These functions are inserted into the evaluated \verb|scope| of the \verb|when| of \verb|keybindings.json| entries.

The language is simply the "script" language of \verb|template-glib| which is not well defined but also not too complex.
It is a fairly obvious scripting language around \verb|GObject Introspection|.

You can import namespaces along with their versions.

\begin{code}{}
require Gtk version "4.0"
\end{code}

Functions are fairly simple in capability and implementation.
Use \verb|def| to define a function and \verb|end| to denote the end of it.
The \verb|return| value is implied based on the last value evaluated in the function.

\begin{code}{}
def hasProject()
  workbench.has_project()
end

def canBuild()
  hasProject() && Ide.BuildManager.from_context(workbench.context).get_can_build()
end
\end{code}

In the example above you'll notice that the \verb|workbench| variable is pre-defined in the scripts scope.
