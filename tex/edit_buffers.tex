\section{Buffers \& BufferAddins}
\label{section:buffer}

The \gidoc{class}{Buffer} class extends \verb|GtkSource.Buffer|.
To make open files available to plug-ins and components the \gidoc{class}{BufferManager} tracks all \verb|Ide.Buffer|.
You can access the \verb|Ide.BufferManager| using \gidoc{type_func}{BufferManager.from_context}.

Many plug-ins want to handle changes to files while the user is editing.
One convenient way to do this is using an \gidoc{iface}{BufferAddin}.
Your addin will be instantiated for each \verb|Ide.Buffer| that is created.
Many virtual methods may be implemented for common actions on a buffer.

The buffer is connected to the object graph using an \gidoc{class}{ObjectBox}.
You can access the \gidoc{class}{Context} from the buffer using \gidoc{method}{Buffer.ref_context}.

\subsection{Change Monitoring}

Builder will automatically watch files for changes when the user is editing them.
This helps ensure that they do not overwrite changes to a file modified externally.
It also helps ensure they know they are editing the newest version of the file.

The \verb|buffer-monitor| plugin provides this feature using \verb|Gio.FileMonitor|.
Some protections are in place to help avoid situation where a file updates at a fast interval.

\begin{info}{}
Note that as of this writing Flatpak does not properly monitor files opened via the document portal.
This does not currently cause problems for Builder in most cases because it does not require the document portal given the level file-system access granted.
It can be a problem with files received via drag-n-drop or opened via \verb|Gio.Application.open_files| or similar.
\end{info}

\subsection{Calculating Diffs}

To display diffs in the line-number gutter Builder must have two versions of the file.
The original version of the file and the most recent edit.

The calculation of diffs is largely left for plug-ins to implement.
A combination of \verb|vcsui| plug-in and the \verb|git| plug-in are used to do diff calculation via \verb|libgit2|.

This information is then accessed via the \gidoc{class}{BufferChangeMonitor} to determine how each run of lines are changed.
The \verb|omni-gutter| plug-in uses this to render line changes in the \gidoc{class}{SourceView} gutter.
The \verb|GtkSource.Map| in the \hyperref[section:editor-page]{Editor Page} also contains a \gidoc{class}{LineChangeGutterRenderer} to display the same line changes.

\subsection{Text Edits}
\label{section:text-edits}

Changes to a buffer can be modeled as a series of "text edits".
You may apply a number of \gidoc{class}{TextEdit} to a single buffer or multiple buffers together using the \gidoc{class}{BufferManager}.
The file need not be open before requesting the edit be applied.
The buffer manager is happy to do that for you.

Many language server integrations use this to apply changes, or "code fixits", to files.

\subsection{Auto-Save}
\label{section:auto-save}

Buffers may be automatically saved after a period of inactivity to reduce the chances of data loss.
See the \verb|auto-save| plug-in for implementation details.

The \gidoc{class}{Buffer} provides API to asynchronously save the document.
The plug-in monitors incoming changes to delay a timeout until an inactivity duration occurs.

Developers may tweak this timeout in preferences to their needs.

\subsection{Trimming Spaces}

It is easy for spaces with empty lines or trailing spaces to crop up in commits.
Builder enables the \verb|trim-spaces| plug-in by default which will remove these extraneous spaces.

In most editors. spaces are trimmed from the entire file.
Builder employs a technique which only trims spaces from modified lines by querying the active \gidoc{iface}{Vcs} version control implementation for changed lines.
This is done indirectly through the use of \gidoc{class}{BufferChangeMonitor}.

\subsection{Updating Copyright}

Some developers feel more at ease when the copyright years are specified in their source code.
In many juristictions providing the year is irrelevant to the copyright being active but it may ease anxieties none-the-less.

The \verb|update-copyright| plug-in can hook into the \verb|pre-save| phase of a document and modify the contents of lines which look like the current user's copyright and effective years.

This is done by checking for email or name of the developer and a single year or range of years.
The end year of a range is replaced with the current year or a range added if there was not one in place.

\subsection{Highlighting Similar Selections}

Text selections exist within the \gidoc{class}{Buffer} rather than the \gidoc{class}{SourceView}.
The \verb|quick-highlight| plug-in provides the ability to highlight other regions of text within a buffer which match the current selection.

As the selection changes a background work item will scan the buffer for text matching the selection and highlight it with a special \verb|Gtk.TextTag|.
