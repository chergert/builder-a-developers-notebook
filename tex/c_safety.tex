\section{Safety in C}

Builder is a large project written in C.
That has both benefits and drawbacks.
A major benefit is that it is easy to integrate all sorts of libraries and tooling while maintaining great performance.
Additionally, it builds upon tooling that is extremely robust.
A major drawback is of course C and the safety pitfalls that come with it.

To help avoid the pitfalls a number of conventions are followed.

We try to be mildly aggressive about released compiler standards which provide safety improvements to the language.

\begin{info}[Code Modernization]
Given that the Builder was created over a number of years not all of the code follows the convention perfectly and has adapted over time.
It is always appreciated when contributers find old crusty code and make it follow newer conventions.
\end{info}

\section{C Programming Conventions}

\subsection{Auto-Pointers}

Try to use the collection of \verb|auto pointer| helpers from GLib and GObject as much as possible.
That includes \verb|g_autoptr()|, \verb|g_auto()|, \verb|g_autofree| and similar.
If you see functions like \verb|g_object_unref()| or \verb|g_free()| called directly there should be a good reason.

\subsection{Ownership Transfer}

Be explicit when transfering ownership of an object or other pointer type using \verb|g_steal_pointer()|.
The compiler is very likely to optimize the cases where it is unnecessary.
In exchange the ownership-transfer is obvious when the next person reads it.

When setting struct fields always try to use the appropriate helper to replace/copy/assign a value.

For example, look at the following string property assignment.
One open codes it which can actually lead to a use-after-free.

\begin{code}{String Field Assignment}
static void
bad_set_field (Object *o,
               const char *val)
{
  if (g_strcmp0 (o->field, val) != 0)
    {
      g_free (o->field);
      /* use of @val could be UAF */
      o->field = g_strdup (val);
      g_object_notify_by_pspec (o, properties[PROP_FIELD]);
    }
}

static void
good_set_field (Object *o,
                const char *val)
{
  if (g_set_str (&o->field, val))
    g_object_notify_by_pspec (o, properties[PROP_FIELD]);
}
\end{code}

Another similar helper is \verb|ide_set_strv()| for \verb|const char * const *| which handles potential UAF as well.

For objects use things like \verb|g_set_object()| or \verb|g_clear_object()|.

\subsection{Clearing Fields to Zero}

It is preferred that you zero fields in structures when freeing the contents.
That helps track down leaks during debugging because we will know what has been freed or not.

\subsection{Final Types}

Use \verb|G_DECLARE_FINAL_TYPE()| and \verb|G_DEFINE_FINAL_TYPE()| when possible.
This allows defining fields within the object struct rather than in an indirected \verb|Private| offset.

\subsection{Interfaces vs Abstract Base Class}

Unless you see a reason that it would make sense to use an interface by default you should lean towards an abstract base class.
Interfaces come with high performance overhead for virtual method dispatch as well as more code to implement.

One reason to choose an interface over a abstract base class is because some plugins would want to use a different base class than \verb|GObject| or \verb|IdeObject|.

\subsection{Bias Towards Private Types}

Try to avoid making API public unless its necessary for plug-ins.
It allows more flexibility in bug fixing without affecting third-party code.

\subsection{API Exporting}

Builder is a single application executable with all plugins statically linked.
Since it allows for external plugins some API is \verb|public| and exported for use by dynamically-loaded shared-modules (also known as \verb|plug-ins|).
Use \verb|IDE_AVAILABLE_IN_ALL| or similar versioned macro to export symbols for plug-ins.

\subsection{Statically Linked Plugins}

All plug-ins bundled with Builder are statically linked into the \verb|gnome-builder| binary.
This allows the kernel's program loader to load the application and all plugins from storage as a sequential I/O request.
This is much faster than loading hundreds of small \verb|.plugin| files only to load their respective \verb|*.so| afterwards.

Consequently, the plug-ins use the "embedded" feature of \verb|libpeas| to register plug-ins at runtime using embedded \verb|GResources| found at \verb|/plugins/*|.

\subsection{Group Header Includes}

Try to group related headers together.
It makes it easier to see how things are used by the source module.

\begin{code}{Example of Header Inclusion}
/* G_LOG_DOMAIN always first */
#define G_LOG_DOMAIN "file-name-without-suffix"

/* include "config.h" always second */
#include "config.h"

/* Includes that are system related, sorted if possible */
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* I18N stands alone */
#include <glib/gi18n.h>

/* GLib/GTK and related includes, sorted if possible */
#include <adwaita.h>
#include <gtk/gtk.h>

/* Includes from Builder's platform libraries */
#include <libide-core.h>
#include <libide-gui.h>

/* Includes from the module directory within builder */
#include "my-widget.h"
#include "my-widget-settings.h"
#include "my-widget-private.h"
\end{code}

\subsection{Functions and Parameters}

The function classification and type belong on their own line.
That includes things like \verb|static| and \verb|inline|.

We align the parameters to a function for the types, \verb|*| sequences, and the name (within reason).
It should be familiar to those who've worked on code in GLib, GTK, and some GNU packages.

\begin{code}{Example of Parameter Alignment}
static gboolean
my_object_func (MyObject            *self,
                const char * const  *thing,
                GError             **error)
\end{code}

Align arguments at the same column When wrapping to a new line.

\begin{code}{Example of Argument Alignment}
  my_object_func (obj,
                  some_really_long_parameter,
                  &error);
\end{code}

\subsection{Code Comments}

Code should written to be as obvious as possible.
But when it is not, or there are preconditions that must be met, write down a little bit about why.

We always use C89-style multi-line comments like \verb|/* */| within Builder.

\subsection{Spacing and Indentation}

Builder uses 2-space indentations (no tabs) for C code.

One of the less commonly known styles is GNU-style for curly braces.
It may look less clean to you but it allows for code to move between GNOME projects more easily, particularly GLib and GTK.


\begin{code}{Example of Indentation}
  if (TRUE)
    {
      /* branch code goes here */
    }

  while (TRUE)
    {
      /* branch code goes here */
    }

  switch (cond)
    {
    case 0:
      break;

    default:
      break;
    }
\end{code}

\subsection{Scoped Blocks}

To avoid issues with GCC's \verb|cleanup| attribute all of our variables are declared at the top of their scope.
It is okay to insert additional scope to make code more readable.

Using \verb|goto| is allowed but you need to be extremely thoughtful about it when breaking out of scope that also has Auto-Pointer usage.
We definitely don't want \verb|goto| interleaved with inline-declarations because they have unpredictable scoping behavior.

See \href{https://blog.fishsoup.net/2015/11/05/attributecleanup-mixed-declarations-and-code-and-goto/}{this post by Owen Taylor} for more information.

\subsection{Integers and Sizes}

We try to use some standard defined types such as \verb|int|, \verb|char|, \verb|double|, and \verb|float|.

Currently, we use GLib-defined types for most other things.

When specifying the number of elements in an array use \verb|gsize| instead of \verb|guint| unless you have a good reason.

\subsection{Tracing Macros}
\label{sec:tracing}

Builder may be compiled with tracing enabled using the Meson build option \verb|-Dtracing=true|.
This enables use of macros like \verb|IDE_ENTER|, \verb|IDE_EXIT|, \verb|IDE_RETURN(val)|, and \verb|IDE_TRACE_MSG|.
Use these macros liberally to help catch issues during Builder development.
They are not to be enabled in \verb|release| builds and therefore have no additional overhead.

Run Builder with the command-line option \verb|-vvvv| when tracing is enabled to see tracing output.
You will see time, log domain, thread number, enter/exit, and additional messages.

\begin{code}{Example of Tracing}
static void
my_object_func (MyObject *self)
{
  IDE_ENTRY;

  g_assert (IDE_IS_MAIN_THREAD ());
  g_assert (MY_IS_OBJECT (self));

  IDE_TRACE_MSG ("Hello, World!");

  IDE_EXIT;
}
\end{code}

\subsection{Address Sanitizer}

Meson has a built-in feature for using Address Sanitizer (ASAN), Undefined Behavior Sanitizer (UBSAN), and Thread Sanitizer (TSAN).
Only one may be enabled at a time.

Using ASAN can catch many errors that are hard to find with other tooling.

Enable ASAN by ensuring you have \verb|libasan| installed and then configuring Builder with \verb|-Db_sanitize=address|.
You can turn it back off by configuring with \verb|-Db_sanitize=none|.

Memory issues don't always come from Builder so sometimes it helps to compile libraries like GTK or GLib with ASAN too.

\subsection{Multi-Process Architecture}

IDEs often need to aggregate a lot of different tooling.
Because of this tracking down memory issues and other bugs can be extremely difficult.

To help protect the Builder application we try hard to isolate external tools into subprocesses.
We then interact with the subprocesses using D-Bus over a private \verb|socketpair()|.

There is already great tooling for compiling D-Bus proxies in the form of \verb|gdbus-codegen| which also makes it easier to write asynchronous code.

A \verb|socketpair()| is used instead of a pipe because it is more flexible in kernel buffering and, more importantly, supports passing file-descriptors when using D-Bus protocol.

\subsection{Asynchronous Code}

Never block the main loop.
Blocking the main loop makes the application stall and that creates a feeling of poor quality software.

There are a few cases in Builder were we do but generally we should avoid it.

New code in Builder is moving towards tooling like \verb|libdex|.
It allows us to use \verb|Future-based| programming styles like developers may be used to in other eco-systems.
One important aspect of it is that it allows for writing synchronous looking code that is asynchronous.

