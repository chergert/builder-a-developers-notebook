\chapter{Threading \& Tasks}

Builder was written from the beginning to be fast and interactive.
That means avoiding complex work on the main thread which should instead focus on drawing the user interface.

To make that a reality much work is deferred until an asynchronous operation completes or performed on a worker thread.

\section{Avoid Gio.Task}

The \verb|GTask| object from GIO contains numerous unfixible issues when used in a multi-threaded environment.
We do not use it and have advocated for deprecation/changes in GIO beacuse of it.

One major issue is that it can cause objects that are only allowed to be disposed on the main application thread to be disposed on another thread.
This could be a \verb|GtkWidget| for example which in turn could do great damage if disposed outside the main thread.

Use \gidoc{class}{Task} instead which handles these types of issues properly.

\section{Ide.Task}

As mentioned in the previous section we do not use \verb|Gio.Task|.
The \gidoc{class}{Task} object provides a nearly identical API but is implemented in such a way that can interact with threads safetly.

The first aspect of this is that \verb|Ide.Task| requires the use of a \verb|GLib.MainContext|.
The second is that the \verb|source_object| of a \verb|Gio.AsyncResult| is passed back to the owning thread along with the result so that it can never be disposed on the worker thread.
Such mechanics are necessary to ensure certain objects are only disposed on the main application thread.

In some cases, it might be even better to restructure your asynchronous code to use \verb|libdex| and \verb|Dex.AsyncResult|.

\subsection{Task Completion}

One problematic aspect of \verb|Gio.Task| is that you do not know when the user completion callback is to be executed when writing the code.
Completion is dependent on various runtime heuristics.
That means you must be extremely careful about what state you keep cached on the stack around completion to avoid many common safety issues.

\gidoc{class}{Task} avoids this by always returning the the \verb|GLib.MainContext| to complete the task so that the user completion callback occurs from a known state.


\section{libdex \& Dex.Future}

Writing asynchronous code in C can be both cumbersome and error prone.
Tracking state across asynchronous callbacks usually results in writing asynchronous, but sequential code, due to the state tracking complexity.

To improve this, I created \verb|libdex| which is a library to make asynchronous programming in C much more convenient.
It is based around the concept of "futures" which many developers are familiar with in other languages.

Libdex includes some niceties around integrating with \verb|io_uring|, go-like \verb|channels|, and combinatorial futures like \verb|all_of| or \verb|any|.

Try to use \verb|libdex| for new asynchronous code as it usually results in something cleaner and easier to read.

For example, you can chain them together easily.

\begin{code}{}
static DexFuture *
wait_seconds_for_signal (int seconds,
                         int signum)
{
  return dex_future_any (
      dex_timeout_new_seconds (seconds),
      dex_unix_signal_new (signum),
      NULL);
}
\end{code}

\subsection{Dex.Fiber}

Writing synchronous looking code that is asynchronous can be much easier to read and reason about.
Further more, it can simplify how state is tracked making it more convenient to do work in parallel.

\verb|libdex| has built-in support for fibers which integrate into the application main loop.
This makes it suitable for doing short amounts of processing on the main thread when the asynchronous operations have completed.

\begin{code}{}
static DexFuture *
find_sdk_by_filter (gpointer user_data)
{
  GomFilter *filter = user_data;
  GError *error = NULL;
  ManualsSdk *sdk;

  /* fiber will suspend via dex_await_object() until the
   * future completes. then it is resumed from main loop.
   */
  sdk = dex_await_object (manuals_repository_find_one (repository, filter), &error);

  if (sdk == NULL)
    return dex_future_new_for_error (error);

  return dex_future_new_take_object (sdk);
}
\end{code}

You can imagine doing many more complex things in fibers such as creating N futures and awaiting on all of their completion.
Where they really shine is when you have multiple "steps" to your asynchronous work and you want to manage that flow in one place instead of split across many functions.

\begin{info}{}
Avoid using \verb|dex_await_*()| functions from callback funtions where you do not control the call path.
It is possible to invalidate thread-local-storage which can be tricky to track down.
This is very rare however.
\end{info}

\subsection{Thread Pools}

\verb|libdex| supports thread pools which combines nicely with \verb|Dex.Fiber|.
Instead of spawning a fiber on the main application thread you can spawn a fiber on the threadpool.
This is useful when you have lots of tedious (but asynchronous) work to do that could process for long enough to make the UI thread jitter.

Thread pools in \verb|libdex| support work-stealing.
Additionally, on Linux, they support \verb|io_uring| for certain types of asynchronous I/O.

You can await on the work of a fiber just like any other future, even when used with a thread pool.

\begin{code}{}
  DexScheduler *scheduler = dex_thread_pool_scheduler_get_default ();
  DexFuture *f = dex_scheduler_spawn (scheduler, my_fiber_func, state, state_free);
  g_autoptr(GError) = NULL;

  if (!dex_await (f, &error))
    g_warning ("%s", error->message);
\end{code}
