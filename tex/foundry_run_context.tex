\chapter{Run Contexts}
\label{chapter:run-contexts}

If you were distill the code foundry into one essential abstraction it would be \gidoc{class}{RunContext}.
It allows you to layer command-line programs, arguments, and environment variables to create subprocesses.

Each layer represents the state of the world for which it's command-line program should see.
Each additional layer stacked atop the previous will see the same environment unless altered.

For example, if you want to run the \verb|ls| command to list directory contents on the host system you first need a layer to represent the host system.
In a new layer you would add \verb|ls| and any arguments or environment variables necessary.

\begin{code}{}
IdeRunContext *run_context = ide_run_context_new ();

/* We want to run this on the host, so "push" a host
 * layer which will trampoline the next layer onto the
 * host system.
 */
ide_run_context_push_host (run_context);

/* We're now in a new layer, so setup our ls command */
ide_run_context_append_argv (run_context, "ls");
ide_run_context_append_argv (run_context, "-lsah");

/* Override LS_COLORS as an example */
ide_run_context_setenv (run_context, "LS_COLORS", "");
\end{code}

What this command may translate into when creating a subprocess looks more like the following:

\begin{shell}{}
$ flatpak-spawn --host --watch-bus env LS_COLORS= ls -lsah
\end{shell}

\section{Layer Callbacks}

Sometimes you need precise control how two layers combine.
To do this use \gidoc{method}{RunContext.push} and provide a callback.
When upper layer is merged down your callback is run and may create a new state of the world for its own layer.

This is extremely helpful when writing tooling for debuggers like \verb|gdb| where you may want to take the upper layer environment and redirect it to \verb|gdb --args|.

Add-ins providing \gidoc{class}{Runtime} should pay special attention to ensure that commands are passed properly into the runtime context.

\subsection{Unix FDs}

The \gidoc{class}{RunContext} also manages the file descriptors for each layer in the form of \gidoc{class}{UnixFDMap}.
At this point you still need to pass FDs to the deepest level through each layer of the stack which means tooling should be aware of FDs and if they should be closed or forwarded deeper.
The \verb|Ide.UnixFDMap| helps you manage that as well as common remappings for streams.

Tooling like Sysprof may pass a FD into a subprocess which is then used to annotate profiling information.
Other tools like \verb|valgrind| can log to a provided file descriptor so that it may be opened in the editor afterwards.
