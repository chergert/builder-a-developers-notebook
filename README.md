# Builder, A Developers Notebook

This is an attempt to document some of how Builder is created and maintained.

## Dependencies

```
sudo dnf install \
    latexmk \
    texlive-iwona \
    texlive-amsmath \
    texlive-listingsutf8 \
    texlive-titling \
    texlive-ulem \
    texlive-parskip \
    texlive-latex-uni8 \
    texlive-ttfutils \
    texlive-metafont \
    texlive-ec \
    texlive-framed \
    texlive-mdframed \
    texlive-tcolorbox \
    texlive-tikzfill \
    texlive-pdfcol \
    texlive-adjustbox \
    texlive-wrapfig
```

